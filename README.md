Những kiến thức cơ bản:

* Flowchart

* Input/Output Basic (Nhập xuất cơ bản)
* Data types (Kiểu dữ liệu)
* Operator (Toán tử)
  * Bit Operator (Phép toán trên bit)
  * Logical/Boolean Operator (Phép toán luận lý)
* Variable (Biến)
  * Variable declaration (Khai báo biến)
  * Global variable(Biến cục bộ)
  * Local Variable(Biến toàn cục)
* Control structure (Cấu trúc điểu khiển)
  * Loop structure (Cấu trúc lặp)
  * Conditional structure(Cấu trúc rẽ nhánh)
* Function (Hàm)
* One-dimensional array (Mảng một chiều)
* Two-dimensional array (Mảng 2 chiều)
* Structures (Kiểu cấu trúc)
* String (Chuỗi ký tự)
* File (Đọc ghi File)

Project

- [Con trỏ - xây dựng Card game]()
- [Đọc ghi file Bitmap]()
- [Thuật toán sắp xếp - Sorting Project](https://gitlab.com/duahau7412/300-bai-code-thieu-nhi/-/tree/main/CSC10004%20-%20Data%20Structures%20%26%20Algorithms/Lab%20Exercises/Lab3%20-%20Project)
