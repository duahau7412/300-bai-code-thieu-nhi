#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
//Viet ham tinh tong cac so chan trong mang bang de quy

int TongChan(int arr[], int n) {
    if (n == 0) {
        return 0;
    }
    int sum = TongChan(arr, n-1);
    if(arr[n-1] % 2 == 0) {
        sum += arr[n-1];
    }
    return sum;
}

int main() {
    int n;
    printf("Chuong trinh tinh tong cac so chan trong mang: ");
    do{
    fflush(stdin);  
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    } while(n <= 0 || n > INT_MAX);
    int* arr = (int*) malloc(n* sizeof(int));
    for(int i = 0; i < n; i++) {
        printf("A[%d] = ",i);
        scanf("%d",&arr[i]);
    }
    
    printf("Array is: ");
    for(int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    int Sn = TongChan(arr, n);
    printf("\nTong chan = %d ",Sn);
    free(arr);
    return 0;
}