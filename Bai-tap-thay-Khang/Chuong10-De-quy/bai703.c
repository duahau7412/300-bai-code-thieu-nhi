#include <stdio.h>

//Tinh T(n) = n! = 1*2*3*...*n bang de quy

int Tn(int n) {
    if(n == 0) {
        return 1;
    } else {
        return n*Tn(n-1);
    }
}

int main() {
    //Tinh T(n) = n! = 1*2*3*...*n bang de quy
    int n = 0;
    printf("Tinh T(n) = n! = 1*2*3*...*n : \n");
    do {
    fflush(stdin);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    }while(n < 0);
    printf("T(n) = %d ",Tn(n));
    return 0;
}