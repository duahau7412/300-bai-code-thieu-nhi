#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
//Viet ham dem so luong da tri duong trong mang
int demSo(int arr[], int n) {
    if(n == 0) {
        return 0;
    }
    int soluong = demSo(arr, n-1);
    if(arr[n-1] > 0) {
        soluong ++;
    }
    return soluong;
}

int main() {
    int n;
    printf("Chuong trinh  dem so luong da tri duong trong mang: ");
    do{
    fflush(stdin);  
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    } while(n <= 0 || n > INT_MAX);
    int* arr = (int*) malloc(n* sizeof(int));
    for(int i = 0; i < n; i++) {
        printf("A[%d] = ",i);
        scanf("%d",&arr[i]);
    }
    printf("Array is: ");
    for(int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n So luong gia tri duong trong mang la: %d",demSo(arr,n));
    return 0;
}