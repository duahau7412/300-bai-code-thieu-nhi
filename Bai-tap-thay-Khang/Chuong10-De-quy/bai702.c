#include <stdio.h>

int Sn(int n) {
    if(n == 1) {
        return 1;
    } else {
        return (n + Sn(n-1));
    }
}


int main() {
    //Tinh S(n) = 1 + 2 + 3 + ... + n. bang de quy
    int n = 0;
    printf("Tinh S(n) = 1 + 2 + 3 + ... + n: \n");
    do {
    fflush(stdin);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    }while(n <= 0);
    printf("S(n) = %d ",Sn(n));
    return 0;
}