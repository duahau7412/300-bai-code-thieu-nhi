//Cho so nguyen duong n. Tong tong cac uoc so nho hon chinh no
#include <stdio.h>

int main() {
    int n;
    int result = 0;
    printf("Cho so nguyen duong n. Tong tong cac uoc so nho hon chinh no ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    printf("Cac uoc so cua so nguyen duong n la: \n");
    for (int i = 1; i < n; i++) {
        if(n % i == 0 ) {
            printf("%d ", i);
            result += i;
        }
    }
    printf("\nSo luong cac 'uoc so chan' cua so nguyen duong n la: %d\n", result);
    return 0;
}