//Tim uoc so LE LON NHAT cua so nguyen duong n
//Vi du n = 100 uoc LE LON NHAT cua 100 la 25
#include <stdio.h>

int main() {
    int n;
    int max = 0;
    printf("Tim uoc so LE LON NHAT cua so nguyen duong n. ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    printf("Cac uoc so cua so nguyen duong n la: \n");
    for (int i = 1; i < n; i++) {
        if(n % i == 0 && i % 2 != 0) {
            printf("%d ", i);
            if (i > max) {
                max = i;
            }
        }
    }
    printf("\nUoc so LE lon nhat la: : %d\n", max);
    return 0;
}