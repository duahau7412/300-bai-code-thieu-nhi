#include <stdio.h>


int main() {
    double sum = 0;
    int n;
    printf("Tinh S(n) = 1/2 + 1/4 + 1/6 +...+ 1/2n \n");
    do{
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    } while (n <= 0);
    for(int i = 1; i <= n; i++) {
        sum += 1.0/(2*i);
    }
    printf("S(n) = %0.5f", sum);
    return 0;
}