//Tinh S(n) = x + x^2/1+2 + x^3/1+2+3 + ... + x^n/1+2+3+...+n
#include <stdio.h>
#include <math.h>

int main() {
    int x,n, temp = 0;
    double result = 0;
    printf("Tinh S(n) = x + x^2/1+2 + x^3/1+2+3 + ... + x^n/1+2+3+...+n \n");
    printf("Vui long nhap x: ");
    scanf("%d", &x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i=1; i <= n; i++) {
        temp +=i;
        result += pow(x,i) / temp;
    }
    printf("S(n) = %0.2f",result);
    return 0;
}