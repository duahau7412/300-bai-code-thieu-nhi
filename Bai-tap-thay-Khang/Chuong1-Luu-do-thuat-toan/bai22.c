//Tinh tich tat ca cac uoc so cua so nguyen duong n
#include <stdio.h>

int main() {
    int n;
    int result = 1;
    printf("Tinh tich tat ca cac uoc so cua so nguyen duong n ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    printf("Cac uoc so cua so nguyen duong n la: \n");
    for (int i = 1; i <= n; i++) {
        if(n % i == 0) {
            printf("%d ", i);
            result *= i;
        }
    }
    printf("\nTich la: %d",result);
    return 0;
}