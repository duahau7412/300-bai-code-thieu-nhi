#include <stdio.h>

int main() {
    int n;
    double sum = 0;
    printf("Tinh S(n) = 1/2 + 3/4 + 5/6 + ... + 2n+1/2n+2 \n");
    do{
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    } while (n < 0);
    for(int i = 0; i <= n; i++) {
        sum += ((2*i)+1*1.0)/((2*i)+2);
    }
    printf("%0.5f", sum);
    return 0;
}