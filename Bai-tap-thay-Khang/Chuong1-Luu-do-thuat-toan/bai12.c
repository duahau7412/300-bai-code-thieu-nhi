//Tinh S(n) = x + x^2 + x^3 + ... + x^n
#include <stdio.h>

int main() {
    int x,n,temp =1 ,result = 0;
    printf("Tinh S(n) = x + x^2 + x^3 + ... + x^n \n");
    printf("Vui long nhap x: ");
    scanf("%d", &x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i = 1; i <= n; i++) {
        temp*=x;
        result += temp;
    }
    printf("S(n) = %d",result);
    return 0;
}