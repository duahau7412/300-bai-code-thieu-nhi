//Kiem tra so nguyen duong n co phai so chinh phuong hay khong
#include <stdio.h>
#include <math.h>
int main() {
    int n, temp;
    
    printf("Kiem tra so nguyen duong n co phai so chinh phuong hay khong. ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    temp = sqrt(n);
    if(sqrt(n) - temp == 0) {
        printf("Day la so chinh phuong: ");
    } else
        printf("Day khong phai la so chinh phuong: ");
    
    return 0;
}