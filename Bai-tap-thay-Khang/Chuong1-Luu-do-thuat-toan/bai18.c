//Tinh S(n) = 1 + x^2/2! + x^4/4! + ... + x^2n/2n!
//Nhan thay, 1 thuc chat la x^2*0/(2*0)!
#include <stdio.h>
#include <math.h>

int giaithua(int n) {
    int temp = 1;
    for(int i=1; i<= n; i++) {
        temp*= i;
    }
    return temp;
}

int main() {
    int x,n,temp=1;
    double result = 0;
    printf("Tinh S(n) = 1 + x^2/2! + x^4/4! + ... + x^2n/2n! \n");
    printf("Vui long nhap x: ");
    scanf("%d",&x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i=0; i <= n; i++) {
        result += pow(x,2*i) / giaithua(2*i);
    }
    printf("S(n) = %0.2f",result);
    return 0;
}
