//Liet ke tat ca cac uoc so LE cua so nguyen duong n
#include <stdio.h>

int main() {
    int n;
    int result = 0;
    printf("Liet ke tat ca cac uoc so LE cua so nguyen duong n ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    printf("Cac uoc so le cua so nguyen duong n la: \n");
    for (int i = 1; i <= n; i++) {
        if(n % i == 0 && i % 2 != 0) {
            printf("%d ", i);
        }
    }
    return 0;
}