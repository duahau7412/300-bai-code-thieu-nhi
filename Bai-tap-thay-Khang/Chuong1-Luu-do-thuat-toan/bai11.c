//Tinh S(n) = 1 + 1*2 + 1*2*3 + ... + 1*2*3*...*n;
//Nhan thay thuc chat S(n) = 1! + 2! + 3! + ... n!
//Nen viet ham tra ve ket qua n! -> tinh duoc S(n)
#include <stdio.h>

int giaithua(int n) {
    int result = 1;
    for(int i=1; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {
    int n;
    double result = 0;
    printf("Tinh S(n) = 1 + 1*2 + 1*2*3 + ... + 1*2*3*...*n \n");
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i = 1; i <= n; i++) {
        result += giaithua(i);
    }
    printf("S(n) = %0.2f",result);
    return 0;
}