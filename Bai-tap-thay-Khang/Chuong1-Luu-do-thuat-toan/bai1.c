#include <stdio.h>
//Tinh S(n) = 1 + 2 + 3 + ... + n
int main() {
    int n, sum = 0;
    printf("Tinh S(n) = 1 + 2 + 3 + ... + n \n");
    do {
        printf("Vui long nhap n: ");
        scanf("%d", &n);
    }while (n <= 0); //Kiem tra tinh dung dan cua n

    for(int i = 1; i <= n; i++) {
        sum += i;
    }
    printf("S(n) = %d", sum);
    return 0;
}