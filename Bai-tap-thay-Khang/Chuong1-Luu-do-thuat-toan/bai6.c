#include <stdio.h>
//Tinh S(n) = 1/1*2 + 1/2*3 + ... + 1/n*(n+1)
int main() {
    int n;
    double sum = 0;
    printf("Tinh S(n) = 1/1*2 + 1/2*3 + ... + 1/n*(n+1) \n");
    do {
        printf("Vui long nhap n: ");
        scanf("%d", &n);
    }while (n <= 0); 
    for(int i = 1; i <= n; i++) {
        sum += 1.0/(i*(i+1));
    }
    printf("S(n) = %0.5f", sum);
    return 0;
}