//Tinh S(n) = x + x^2/2! + x^3/3! + ... + x^n/n!
#include <stdio.h>
#include <math.h>

int main() {
    int x,n, temp = 1;
    double result = 0;
    printf("Tinh S(n) = x + x^2/2! + x^3/3! + ... + x^n/n! \n");
    printf("Vui long nhap x:");
    scanf("%d", &x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i=1; i <= n; i++) {
        temp*= i;
        result += pow(x,i)/temp;
    }
    printf("S(n) = %0.2f", result);
    return 0;
}
