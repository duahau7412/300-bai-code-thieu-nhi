#include <stdio.h>

int main() {
    int n;
    double sum = 0;
    printf("Tinh S(n) = 1/2 + 2/3 + 3/4 + ... + n/n+1 \n");
    do{
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    } while (n <= 0);
    for(int i = 1; i <= n; i++) {
        sum += i*1.0/(i+1);
    }
    printf("%0.5f", sum);
    return 0;
}