#include <stdio.h>


int main() {
    double sum = 0;
    int n;
    printf("Tinh S(n) = 1 + 1/2 + 1/3 +...+ 1/n \n");
    do{
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    } while (n <= 0);
    for(int i = 1; i <= n; i++) {
        sum += 1.0/i;
    }
    printf("S(n) = %0.5f", sum);
    return 0;
}