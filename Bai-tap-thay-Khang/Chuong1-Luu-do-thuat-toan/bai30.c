//Kiem tra so nguyen duong co phai so hoan thien khong
//So hoan thien la so nguyen duong ma tong cac so nguyen duong cua no (tru chinh no) bang no
#include <stdio.h>

int main() {
    int n;
    int result = 0;
    printf("Kiem tra so nguyen duong co phai so hoan thien khong. ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    for (int i = 1; i < n; i++) {
        if(n % i == 0 ) {
            result += i;
        }
    }
    if(result == n) {
        printf("Day la so hoan thien: ");
    } else {
        printf("Day khong phai la so hoan thien: ");
    }
    return 0;
}