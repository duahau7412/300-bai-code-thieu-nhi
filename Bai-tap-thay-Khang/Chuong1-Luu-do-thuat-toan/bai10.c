#include <stdio.h>
//Tinh T(x,n) = x^(n)
int main() {
    int n,x;
    int sum = 1;
    printf("Tinh T(x,n) = x^(n) \n");
    do {
        printf("Vui long nhap x: ");
        scanf("%d", &x);
        printf("Vui long nhap n: ");
        scanf("%d", &n);
    }while (x < 0 || n < 0); //Kiem tra tinh dung dan cua n
    
    if(n == 0 && x == 0) {
        printf("Loi phep tinh ");
        return 0;
    }

    for(int i = 1; i <= n; i++) {
        sum *= x;
    }
    printf("T(x,n) = %d", sum);
    return 0;
}