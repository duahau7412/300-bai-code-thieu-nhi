//Tinh S(n) = 1 +  1/(1+2) + 1/(1+2+3)+ ... + 1/(1+2+3+...+n)
#include <stdio.h>

int main() {
    int n, temp = 0;
    double result = 0;
    printf("Tinh S(n) = 1 +  1/(1+2) + 1/(1+2+3)+ ... + 1/(1+2+3+...+n) \n");
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i = 1; i <= n; i++) {
        temp +=i;
        result += 1.0/temp; //Nho phai ep kieu
    }
    printf("S(n) = %0.3f ", result);
    return 0;
}