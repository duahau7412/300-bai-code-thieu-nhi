//Tinh S(n) = x^2 + x^4 + ⋯ + x^2n
//Tu bai nay su dung thu vien math.h -> De dung ham pow(x,n) tinh so mu cho tien
#include <stdio.h>
#include <math.h> //use pow(int,int)

int main() {
    int x,n; 
    double result = 0;
    printf("Tinh S(n) = x^2 + x^4 + ⋯ + x^2n \n");
    printf("Vui long nhap x: ");
    scanf("%d", &x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i = 1; i <= n; i++) {
        result += pow(x,2*i);
    }
    printf("S(n) = %0.2f\n",result);
    return 0;
}
