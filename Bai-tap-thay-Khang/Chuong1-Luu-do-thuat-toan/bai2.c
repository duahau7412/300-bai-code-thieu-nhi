#include <stdio.h>


int main() {
    int sum = 0, n;
    printf("Tinh S(n) = 1^(2) + 2^(2) + 3^(2) + ... n^(2) \n");
    do{
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    } while (n <= 0);
    for(int i = 1; i <= n; i++) {
        sum += i*i;
    }
    printf("S(n) = %d", sum);
    return 0;
}