//Liet ke tat ca cac uoc so cua so nguyen duong n
#include <stdio.h>

int main() {
    int n;
    printf("Liet ke tat ca cac uoc so cua so nguyen duong n ");
    printf("\nVui long nhap n: ");
    scanf("%d", &n);
    printf("Cac uoc so cua so nguyen duong n la: \n");
    for (int i = 1; i <= n; i++) {
        if(n % i == 0) {
            printf("%d ", i);
        }
    }
    return 0;
}