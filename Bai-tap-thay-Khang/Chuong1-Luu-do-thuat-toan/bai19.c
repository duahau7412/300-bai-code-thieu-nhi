//Tinh S(n) = 1 + x + x^3/3! + ... + x^2n+1/(2n+1)!
#include <stdio.h>
#include <math.h>

int giaithua(int n) {
    int temp = 1;
    for(int i=1; i<= n; i++) {
        temp*= i;
    }
    return temp;
}

int main() {
    int x,n,temp=1;
    double result = 0;
    printf("Tinh S(n) = 1 + x + x^3/3! + ... + x^2n+1/(2n+1)! \n");
    printf("Vui long nhap x: ");
    scanf("%d",&x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    result ++;
    for(int i=0; i <= n; i++) {
        result += pow(x,2*i+1) / giaithua(2*i+1);
    }
    printf("S(n) = %0.2f",result);
    return 0;
}