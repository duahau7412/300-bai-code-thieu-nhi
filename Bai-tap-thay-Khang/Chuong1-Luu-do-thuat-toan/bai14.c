//Tinh S(n) = x + x^3 + x^5 + ... + x^2n+1
#include <stdio.h>
#include <math.h>
int main() {
    int x,n, result = 0;
    printf("Tinh S(n) = x + x^3 + x^5 + ... + x^2n+1 \n");
    printf("Vui long nhap x: ");
    scanf("%d", &x);
    printf("Vui long nhap n: ");
    scanf("%d", &n);
    for(int i = 0; i <= n; i++) {
        result += pow(x,2*i+1);
    }
    printf("S(n) = %d", result);
    return 0;
}