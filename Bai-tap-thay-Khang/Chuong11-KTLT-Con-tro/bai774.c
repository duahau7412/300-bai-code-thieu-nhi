/* Hay cho biet ket qua thuc hien cua doan chuong trinh duoi day
int a;
int *p;
a = 5;
p = &a;
a++;
printf("\nGia tri cua bien a: %d\n", a); -> output: 6
printf("\n Gia tri tai dia chi cua bien con tro p dang cho toi: %d",*p); -> output: 6 vi p dang cho den a ma a dang la 6
*/
#include <stdio.h>
int main() {
int a;
int *p;
a = 5;
p = &a;
a++;
printf("\nGia tri cua bien a: %d\n", a);
printf("Gia tri tai dia chi cua bien con tro p dang cho toi: %d",*p);
return 0;
}