/*
Hay cho biet doan chuong trinh day day cau lenh nao dung cau lenh nao sai
int a;
int *p;
a = 5;
p = 7; -> Day la cau lenh sai vi chua gan dia chi bo nho cho bien p, va muon truy cap gia tri o dia chi o nho thi phai dung toan tu *
Sua lai thanh
p = &a;
*p = 7;
*/
#include <stdio.h>

int main() {
    int a;
    int *p;
    a  = 5;
    p = &a;
    *p = 7;
    return 0;
}