//Viet hoa hoan vi 2 so nguyen bang cach su dung ky thuat con cho
#include <stdio.h>
void swap(int *a, int* b) {
    int c = *a;
    *a = *b;
    *b = c;
}

int main() {
    int a = 5, b= 10;
    printf("Before A = %d B = %d",a,b);
    swap(&a,&b);
    printf("\nAfter A = %d B = %d",a,b);
    return 0;
}