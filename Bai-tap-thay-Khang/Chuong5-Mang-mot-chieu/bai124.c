//Viet ham kiem tra trong mang cac so nguyen
//Co ton tai gia tri chan nho hon 2004 hay khong ?
#include <stdio.h>
#include <stdbool.h>
bool checkChanvaBeHon2004(int arr[], int n) {
    
    for(int i = 0; i < n; i++) {
        if(arr[i] % 2 == 0 && arr[i] < 2004) {
            return true;
        }
    }
    return false;
}

int main() {
    int ex[] = {8,5,3,6,9,11,3,4,7};
    int n = sizeof(ex)/sizeof(ex[0]);
    if(checkChanvaBeHon2004(ex,n)) {
        printf("Ton tai gia tri chan < 2004 ");
    } else {
        printf("Khong ton tai gia tri chan < 2004 ");
    }
    return 0;
}