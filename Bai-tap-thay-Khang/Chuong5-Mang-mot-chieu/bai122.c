//Tim gia tri lon nhat trong mang 1 chieu cac so thuc
#include <stdio.h>

int main() {
    double ex[] = {8,7.7,3.2,6.8,9.5,11.5,11.3,4.4,5.0};
    double max = ex[0];
    for(int i = 1; i < sizeof(ex)/sizeof(ex[0]); i++) {
        if(ex[i] > max) {
            max = ex[i];
        }
    }
    printf("Gia tri lon nhat trong mang 1 chieu la: %0.2f ",max);
    return 0;
}