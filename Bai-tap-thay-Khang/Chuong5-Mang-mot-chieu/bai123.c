//Tim VI TRI gia tri nho nhat trong mang 1 chieu cac so nguyen
#include <stdio.h>

int main() {
    int ex[] = {8,5,3,6,9,11,3,4,7};
    int min = ex[0];
    int pos;
    for(int i = 1; i < sizeof(ex)/sizeof(ex[0]); i++) {
        if(ex[i] < min) {
            min = ex[i];
            pos = i;
        }
    }
    printf("VI TRI gia tri nho nhat trong mang 1 chieu la: %d ",pos);
    return 0;
}