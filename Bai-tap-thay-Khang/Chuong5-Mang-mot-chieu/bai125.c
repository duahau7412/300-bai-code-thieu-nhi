//Viet ham dem so luong so nguyen to nho hon 100 tron mang
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool checkPrime(int number ) {
    if (number < 2) {
        return false;
    }
    for(int i = 2; i <= sqrt(number); i++) {
        if(number % i == 0) {
            return false;
        }
    }
    return true;
}
int countPrimeLess100(int arr[], int n) {
    int count = 0;
    for(int i = 0; i < n; i++) {
        if(checkPrime(arr[i]) && arr[i] < 100) {
            count++;
        }
    }
    return count;
}

int main() {
    int ex[] = {8,5,3,6,9,11,3,4,7,101};
    int n = sizeof(ex)/sizeof(ex[0]);
    printf("So luong so nguyen to nho hon 100 trong mang la: %d ",countPrimeLess100(ex, n));
    return 0;
}