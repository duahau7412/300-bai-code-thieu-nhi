﻿using System;

namespace HomeWork_2_01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Write a program to prompt the user to input threeinteger values and print these values in forward and reversed order
            int []num = new int [3];
            Console.WriteLine("Input the first value: ");
            num[0] = int.Parse(Console.ReadLine());
            Console.WriteLine("Input the second value: ");
            num[1]= Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input the third value: ");
            num[2] = Convert.ToInt32(Console.ReadLine());
            Array.Sort(num);
            Console.WriteLine($"Backward order: {num[2]} {num[1]} {num[0]}");
            Console.WriteLine($"Forward order: {num[0]} {num[1]} {num[2]}");
            
        }

    }
}
