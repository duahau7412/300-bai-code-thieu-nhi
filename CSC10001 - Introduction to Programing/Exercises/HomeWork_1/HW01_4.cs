using System;
namespace HomeWork_1_04 {
    class Program {
        static void Main(string[] args) {
            //Write a program that accepts two integers from the user and calculates thesum of these integers
            int num1, num2;
            Console.WriteLine("Input the first integer: ");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Input the second integer: ");
            num2 = int.Parse(Console.ReadLine());
            int sum = num1 + num2;
            Console.WriteLine($"Sum of the above two integers = {sum}");

        }
    }

}