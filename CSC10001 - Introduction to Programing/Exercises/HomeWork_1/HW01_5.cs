using System;
namespace HomeWork_1_05 {
    class Program{
        static void Main(String[] args) {
            /*
            Write a program that accepts the information of two productsfrom the user,  
            for each of which that includesthe unit price (a  floating  point  number) 
            and the number of purchases (an integer number), 
            and calculates the average value for each product item.  
            */
            double productPrice1, productPrice2;
            int numItemsProduct1, numItemsProduct2;
            Console.WriteLine("Enter Unit price –Product 1: ");
            productPrice1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter No. of item–Product 1: ");
            numItemsProduct1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Unit price –Product 2: ");
            productPrice2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter No. of item–Product 2: ");
            numItemsProduct2 = int.Parse(Console.ReadLine());
            double averageValue = (productPrice1*numItemsProduct1+productPrice2*numItemsProduct2)/(numItemsProduct1+numItemsProduct2);
            averageValue = Math.Round(averageValue,6);
            Console.WriteLine("Average Value = " + averageValue);

        }
    }
}