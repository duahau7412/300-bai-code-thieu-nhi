using System;
namespace HomeWork_1_03 {
    class Program {
        static void Main(string[] args) {
            //Write a program to compute the perimeter and area of a circle with a radius of 6 inches
            int r = 6;
            double chuvi = 2*r*Math.PI, dientich = Math.PI * Math.Pow(r,2);
            chuvi = Math.Round(chuvi,2); //Lam tron so
            dientich = Math.Round(dientich,2);
            Console.WriteLine($"Perimeter of the Circle = {chuvi} inches");
            Console.WriteLine($"Area of the Circle = {dientich} square inches");
        }
    }
}