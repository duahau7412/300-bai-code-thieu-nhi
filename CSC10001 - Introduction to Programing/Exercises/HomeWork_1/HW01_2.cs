﻿using System;

namespace HomeWork_1_02
{
    class Program
    {
        static void Main(string[] args)
        {   
            int chuvi = (7+5)*2, dientich = 7*5;
            Console.WriteLine($"Perimeter of the rectangle = {chuvi} inches\nArea of the rectangle = {dientich} square inches");
        }
    }
}
